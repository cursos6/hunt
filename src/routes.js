import {createStackNavigator} from 'react-navigation';

import Main from './pages/main';

export default createStackNavigator(
  {
    Main,
  },
  {
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#DA000F',
      },
      headerTintColor: '#FFF',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  },
);
